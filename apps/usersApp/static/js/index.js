function openNav(){
    document.getElementById("myNav").style.width = "100%";
}
function closeNav(){
    document.getElementById("myNav").style.width = "0%";
}

var textWrapper = document.querySelector('.ml2');
textWrapper.innerHTML = textWrapper.textContent.replace(/\s/g, "<span class='letter'>$&</span>");

anime.timeline({loop: true})

.add ({
    targets: '.ml2 .letter',
    scale: [4, 1],
    opacity: [0, 1],
    translateZ: 0,
    easing: "easeOutExpo",
    duration: 950,
    delay: (el, i) => 70*i
}).add({
    targets: '.ml2',
    opacity: 0,
    duration: 1000,
    easing: "easeOutExpo",
    duration: 1000,
    easing: "easeOutExpo",
    delay: 1000
});


window.onscroll = function() {
    var theta = document.documentElement.scrollTop / 50 % Math.PI;

document.getElementById('item1').style.transform ='rotate(' + theta + 'rad)';
document.getElementById('item2').style.transform ='rotate(' + theta + 'rad)';
document.getElementById('item3').style.transform ='rotate(' + theta + 'rad)';
}


$(document).ready(function(){
	$('.overlay').click(function(){
		$('#myNav').slideToggle(300);
		$('.top').toggleClass('rotate');
		$('.middle').toggleClass('rotate-back');
		$('.menu-name').toggleClass('bump');
		$('.bg-cover').toggleClass('reveal');
	});
	$('.bg-cover').click(function(){
		$('#menu').slideToggle(300);
		$('.top').toggleClass('rotate');
		$('.middle').toggleClass('rotate-back');
		$('.menu-name').toggleClass('bump');
		$('.bg-cover').toggleClass('reveal');
	})
});
