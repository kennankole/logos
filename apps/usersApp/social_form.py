from allauth.socialaccount.forms import SignupForm
from django import forms
from django.urls import reverse_lazy
from django.utils.text import slugify

from . import models


class LogosCustomSocialSignUpForm(SignupForm):
    USER_TYPES = (
        ('Mentor', "Mentor"),
        ("Tertiary", "Tertiary"),
        ("NonTertiary", "NonTertiary"),
        ("Alumni", "Alumni"),
    )

    user_type = forms.CharField(max_length=50, widget=forms.Select(choices=USER_TYPES))

    def save(self, request):
        user = super(LogosCustomSocialSignUpForm, self).save(request)
        user.user_type = self.cleaned_data['user_type']

        # creating userprofile upon social registrations
        if self.cleaned_data['user_type'] == "Mentor":
            models.MentorProfile.objects.update_or_create(user=user, defaults={'slug':slugify(user.email)})
        elif self.cleaned_data['user_type'] == "Tertiary":
            models.TertiaryProfile.objects.update_or_create(user=user, defaults={'slug': slugify(user.email)})
        elif self.cleaned_data['user_type'] == "NonTertiary":
            models.NonTertiaryProfile.objects.update_or_create(user=user, defaults={'slug': slugify(user.email)})
        elif self.cleaned_data['user_type'] == "Alumni":
            models.AlumniProfile.objects.update_or_create(user=user, defaults={'slug': slugify(user.email)})
        
        # saving the user instance
        user.save()

        # return the user instance. 
        return user


