from allauth.account.views import SignupView
from django.contrib.auth import get_user
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404, render
from django.urls import reverse, reverse_lazy
from django.utils.decorators import method_decorator
from django.views import generic

from apps.usersApp.utils import ProfileGetObjectMixin

from . import forms, models, social_form

# Create your views here.

class RegistrationView(SignupView):
    template_name = 'account/signup.html'
    form_class = forms.UserRegistrationForm


# Tertiary Profile
@method_decorator(login_required, name='dispatch')
class TertiaryProfileDetailView(ProfileGetObjectMixin, generic.DetailView):
    model = models.TertiaryProfile
    template_name = 'usersApp/tertiaryprofile_detail.html'

@method_decorator(login_required, name='dispatch')
class TertiaryProfileUpdateView(ProfileGetObjectMixin, generic.UpdateView):
    form_class = forms.TertiaryProfileUpdateForm

class TertiaryPublicProfileView(generic.DetailView):
    model = models.TertiaryProfile



# Mentor Profile
@method_decorator(login_required, name='dispatch')
class MentorProfileDetailView(ProfileGetObjectMixin,generic.DetailView):
    model = models.MentorProfile
    template_name = "usersApp/mentorprofile_detail.html"
    
@method_decorator(login_required, name='dispatch')
class MentorUpdateProfileView(ProfileGetObjectMixin, generic.UpdateView):
    form_class = forms.MentorProfileUpdateForm

class MentorPublicProfileView(generic.DetailView):
    model = models.MentorProfile



# NonTertiary Profile 
@method_decorator(login_required, name='dispatch')
class NonTertiaryProfileDetailView(ProfileGetObjectMixin, generic.DetailView):
    model = models.NonTertiaryProfile
    template_name = "usersApp/nontertiaryprofile_detail.html"

@method_decorator(login_required, name='dispatch')
class NonTertiaryProfileUpdateView(ProfileGetObjectMixin, generic.UpdateView):
    form_class = forms.NonTertiaryProfileUpdateForm

class NonTertiaryPublicProfileView(generic.DetailView):
    model = models.NonTertiaryProfile



# Alumni Profiles 
@method_decorator(login_required, name='dispatch')
class AlumniProfileDetailView(ProfileGetObjectMixin, generic.DetailView):
    model = models.AlumniProfile
    template_name = "usersApp/alumniprofile_detail.html"

@method_decorator(login_required, name='dispatch')
class AlumniProfileUpdateView(ProfileGetObjectMixin, generic.UpdateView):
    form_class = forms.AlumniProfileUpdateForm

class AlumniPublicProfileView(generic.DetailView):
    model = models.AlumniProfile




