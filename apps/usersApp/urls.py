from django.urls import path

from . import views

app_name = 'usersApp'
urlpatterns = [
    path('signup/', views.RegistrationView.as_view(), name='signup'),
    
    # Mentor Profiles 
    path('mentor_detail/', views.MentorProfileDetailView.as_view(), name='mentor_detail'),
    path('mentor/public/profile/<slug:slug>/', views.MentorPublicProfileView.as_view(), name='mentor_public_profile'),
    path('mentor/profile/update/', views.MentorUpdateProfileView.as_view(), name='mentor_update_profile'),

    # Tertiary Profiles 
    path('tertiary_detail/', views.TertiaryProfileDetailView.as_view(), name='tertiary_detail'),
    path('tertiary/public/profile/<slug:slug>/', views.TertiaryPublicProfileView.as_view(), name='tertiary_public_profile'),
    path('tertiary/profile/update/', views.TertiaryProfileUpdateView.as_view(), name='tertiary_update_profile'),

    # Non Tertiary Profiles
    path('nontertiary_detail/', views.NonTertiaryProfileDetailView.as_view(), name='nontertiary_detail'),
    path('nontertiary/public/profile/<slug:slug>/', views.NonTertiaryPublicProfileView.as_view(), name='nontertiary_public_profile'),
    path('nontertiary/profile/update/', views.NonTertiaryProfileUpdateView.as_view(), name='nontertiary_profile_update'),

    # Alumni Profiles 
    path('alumni_detail/', views.AlumniProfileDetailView.as_view(), name='alumni_detail'),
    path('alumni/public/profile/<slug:slug>/', views.AlumniPublicProfileView.as_view(), name='alumni_public_profile'),
    path('alumni/profile/update/', views.AlumniProfileUpdateView.as_view(), name='alumni_profile_update'),

]
