from allauth.account.forms import SignupForm
from django import forms
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from django.contrib.postgres.fields import array
from django.forms import widgets
from django.forms.widgets import Widget
from django.utils.text import slugify

from . import models


class CustomUserCreationForm(UserCreationForm):

    class Meta:
        model = models.CustomUser
        fields = ('email', )


class CustomUserChangeForm(UserChangeForm):

    class Meta:
        model = models.CustomUser
        fields = ('email',)


class UserRegistrationForm(SignupForm):
    USER_TYPES = (
        ('Mentor', "Mentor"),
        ("Tertiary", "Tertiary"),
        ("NonTertiary", "NonTertiary"),
        ("Alumni", "Alumni"),
    )

    user_type = forms.CharField(max_length=50, widget=forms.Select(choices=USER_TYPES))

    class Meta:
        model = models.CustomUser
        fields = ('email', 'password1', 'password2', 'user_type')

    def save(self, request):
        user = super(UserRegistrationForm, self).save(request)
        user.user_type = self.cleaned_data['user_type']
        if self.cleaned_data['user_type'] == 'Mentor':
            models.MentorProfile.objects.update_or_create(user=user, defaults={'slug':slugify(user.email)})
        elif self.cleaned_data['user_type'] == 'Tertiary':
            models.TertiaryProfile.objects.update_or_create(user=user, defaults={'slug':slugify(user.email)})
        elif self.cleaned_data['user_type'] == 'NonTertiary':
            models.NonTertiaryProfile.objects.update_or_create(user=user, defaults={'slug':slugify(user.email)})
        elif self.cleaned_data['user_type'] == 'Alumni':
            models.AlumniProfile.objects.update_or_create(user=user, defaults={'slug':slugify(user.email)})
        user.save()  
        return user


class MentorProfileUpdateForm(forms.ModelForm):
    
    class Meta:
        model = models.MentorProfile
        fields = (
            'occupation', 'about',
            'photo',

        )

class TertiaryProfileUpdateForm(forms.ModelForm):
    date_of_birth = forms.DateField(widget=forms.DateInput(attrs={
        'type':'date'
    }))
    year_of_graduation = forms.DateField(widget=forms.DateInput(attrs={'type':'date'}))

    class Meta:
        model = models.TertiaryProfile
        fields = (
            'date_of_birth', 'gender', 'birth_certificate',
            'photo', 'about', 'contacts',
            'parents', 'parents_or_guardian_contacts',
            'kcse_results_slip', 'transcript',
            'course','type_of_course',
            'type_of_institution','name_of_school', 'county',
            'helb_beneficiary', 'fees_structure',
            'year_of_graduation', 
        
        )

class NonTertiaryProfileUpdateForm(forms.ModelForm):

    date_of_birth = forms.DateField(widget=forms.DateInput(attrs={'type':'date'}))
    year_of_national_examination = forms.DateField(widget=forms.DateInput(attrs={'type':'date'}))

    class Meta:
        model = models.NonTertiaryProfile
        fields = (
            'date_of_birth', 'gender', 'birth_certificate',
            'photo', 'about',
            'parents', 'parents_or_guardian_contacts',
            'kcpe_results_slip', 'report_form',
            'name_of_school', 'county',
            'primary_or_secondary', 'fees_structure',
            'year_of_national_examination', 
        )

class AlumniProfileUpdateForm(forms.ModelForm):
    year_of_graduation = forms.DateField(widget=forms.DateInput(attrs={'type':'date'}))

    class Meta:
        model = models.AlumniProfile
        fields = (
            'occupation','about',
            'study_level', 'year_of_graduation',
            'photo'
        )