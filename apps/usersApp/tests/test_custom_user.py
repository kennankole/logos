import pytest
from django.contrib.auth import get_user_model
from django.test import TestCase
from mixer.backend.django import mixer

# In order to save data into the database using 
# test_models.py we add the code below.
pytestmark = pytest.mark.django_db

# Testing custom user manager model
class UserMangersTests(TestCase):

    def test_create_user(self):
        User = get_user_model()
        user = User.objects.create_user(email='kennankole@idea.com', password='fooo')
        self.assertEqual(user.email, "kennankole@idea.com")
        self.assertTrue(user.is_active)
        self.assertFalse(user.is_staff)
        self.assertFalse(user.is_superuser)
        
        try:
            '''
            username is None for the AbstractUser option
            username does not exist for the AbstractBaseUser option
            '''
            self.assertIsNone(user.username)
        except AttributeError:
            pass
        with self.assertRaises(TypeError):
            User.objects.create_user()
        with self.assertRaises(TypeError):
            User.objects.create_user(email="")
        with self.assertRaises(ValueError):
            User.objects.create_user(email='', password='fooo')
            
    
    def test_create_superuser(self):
        User = get_user_model()
        
        admin_user = User.objects.create_superuser("ankosuper@idea.com", 'fooo')
        self.assertEqual(admin_user.email, "ankosuper@idea.com")
        self.assertTrue(admin_user.is_active)
        self.assertTrue(admin_user.is_staff)
        self.assertTrue(admin_user.is_superuser)

        try:
            '''
            username is None for AbstractBaseUser option
            username does not exist for the AbstracBaseUser option
            '''
            self.assertIsNone(admin_user.username)
        except AttributeError:
            pass
        with self.assertRaises(ValueError):
            User.objects.create_superuser(email='ankosuper@idea.com' , password='fooo', is_superuser=False)


