import pytest
from django.test import TestCase
from django.urls import reverse
from mixer.backend.django import mixer

from apps.usersApp import models

pytestmark = pytest.mark.django_db

class TestUserProfile(TestCase):

    @classmethod
    def setUpTestData(cls):
        # Mentor
        cls.mentor = mixer.blend(models.CustomUser, user_type="Mentor")
        cls.mentor_p = mixer.blend(models.MentorProfile, slug="weorjo")
        cls.mentor.save()
        cls.mentor_p.save()
     
        # Tertiary 
        cls.tertiary = mixer.blend(models.CustomUser, user_type="Tertiary")
        cls.tertiary_p = mixer.blend(models.TertiaryProfile, slug="dmoare") 
        cls.tertiary.save()
        cls.tertiary_p.save()

        # Alumni
        cls.alumni = mixer.blend(models.CustomUser, user_type="Alumni")
        cls.alumni_ = mixer.blend(models.AlumniProfile, slug="dfdofieo")
        cls.alumni.save()
        cls.alumni_.save()
       
        # Non Tertiary 
        cls.non_tertiary = mixer.blend(models.CustomUser, user_type="NonTertiary")
        cls.non_tertiary_ = mixer.blend(models.NonTertiaryProfile, slug='dsdkfeor')
        cls.non_tertiary.save()
        cls.non_tertiary_.save()

class MentorProfileTestCase(TestUserProfile):
      
    def test_mentorprofile_model_exist(self):
        self.assertEqual(self.mentor.user_type, "Mentor")

    def test_absolute_url(self):
        self.assertEqual(self.mentor_p.get_absolute_url(), reverse("usersApp:mentor_public_profile", args=[str(self.mentor_p.slug)]))

    def test_update_url(self):
        self.assertEqual(self.mentor_p.get_update_url(), reverse("usersApp:mentor_update_profile"))
    
    def test_string_representation(self):
        self.assertEqual(str(self.mentor_p.occupation), self.mentor_p.occupation)
    
        
class TertiaryProfileTestCase(TestUserProfile):

    def test_tertiaryprofile_model_exists(self):
        self.assertEqual(self.tertiary.user_type, "Tertiary")

    def test_absolute_url(self):
        self.assertEqual(self.tertiary_p.get_absolute_url(), reverse("usersApp:tertiary_public_profile", args=[str(self.tertiary_p.slug)]))
    
    def test_update_url(self):
        self.assertEqual(self.tertiary_p.get_update_url(), reverse("usersApp:tertiary_update_profile"))

    def test_string_representation(self):
        self.assertEqual(str((self.tertiary_p.name_of_school) + (self.tertiary_p.course)), (self.tertiary_p.name_of_school + self.tertiary_p.course))
class AlumniProfileTestCase(TestUserProfile):

    def test_alumniprofile_model_created(self):
        self.assertEqual(self.alumni.user_type, "Alumni")

    def test_absolute_url(self):
        self.assertEqual(self.alumni_.get_absolute_url(), reverse("usersApp:alumni_public_profile", args=[str(self.alumni_.slug)]))

    def test_get_update_url(self):
        self.assertEqual(self.alumni_.get_update_url(), reverse("usersApp:alumni_profile_update"))

    def test_string_representation(self):
        self.assertEqual(str(self.alumni_.occupation), self.alumni_.occupation)

class NonTertiaryProfileTestCase(TestUserProfile):

    def test_nontertiaryprofile_model_created(self):
        self.assertEqual(self.non_tertiary.user_type, "NonTertiary")

    def test_absolute_url(self):
        self.assertEqual(self.non_tertiary_.get_absolute_url(), reverse("usersApp:nontertiary_public_profile", args=[str(self.non_tertiary_.slug)]))

    def test_get_update_url(self):
        self.assertEqual(self.non_tertiary_.get_update_url(), reverse("usersApp:nontertiary_profile_update"))

    def test_string_representaiont(self):
        self.assertEqual(str(self.non_tertiary_.name_of_school), self.non_tertiary_.name_of_school)