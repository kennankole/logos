import unittest

from django.test import Client
from selenium import webdriver


class SimpleTest(unittest.TestCase):
    def test_clients(self):
        client = Client()
        response = client.get("/ping/")
        self.assertEqual(response.status_code, 200)
