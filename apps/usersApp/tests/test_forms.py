import django
import pytest
from django.contrib.auth import get_user
from django.test import Client, RequestFactory, TestCase
from django.urls import reverse
from mixer.backend.django import mixer

from apps.usersApp import forms, models, social_form, views
from apps.usersApp.models import TertiaryProfile

pytestmark = pytest.mark.django_db

class SignUpTestCase(TestCase):

    @classmethod
    def setUpTestData(cls):
        # create various user types 
        cls.user_m = mixer.blend(models.CustomUser, user_type="Mentor")
        cls.user_t = mixer.blend(models.CustomUser, user_type="Tertiary")
        cls.user_nt = mixer.blend(models.CustomUser, user_type="NonTertiary")
        cls.user_al = mixer.blend(models.CustomUser, user_type="Alumni")
       


class MentorSignUpFormTestCase(SignUpTestCase):
  
    def test_mentor_form_invalid(self):
        form = forms.UserRegistrationForm(data={})

        assert form.is_valid() is False, ("Should ne invalid if no data is given")

    def test_mentor_form_is_valid(self):
        response = self.client.post(reverse('account_signup'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, template_name="account/signup.html")
        
        
        data = {
            "email": 'tet@idea.com',
            "password1": self.user_m.password,
            "password2": self.user_m.password,
            "user_type": self.user_m.user_type,
        }
        form = forms.UserRegistrationForm(data=data)
        self.assertTrue(form.is_valid())
        response = self.client.post(reverse('account_signup'), data=data)
        print(form.errors)
        self.assertEqual(self.user_m.user_type, "Mentor")
        self.assertEqual(models.MentorProfile.objects.count(), 1)
   

class NonTertiarySignUpFormTestCase(SignUpTestCase):

    def test_nontertiary_form_invalid(self):
        form = forms.UserRegistrationForm(data={})

        assert form.is_valid() is False, ("Should ne invalid if no data is given")

    def test_nontertiary_form_is_valid(self):
        response = self.client.post(reverse('account_signup'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, template_name="account/signup.html")
        
        data = {
            "email": "nont@idea.com",
            "password1": self.user_nt.password,
            "password2": self.user_nt.password,
            "user_type": self.user_nt.user_type,
        }
        form = forms.UserRegistrationForm(data=data)
        print(form.errors)
        self.assertTrue(form.is_valid())
        response = self.client.post(reverse('account_signup'), data=data)
        self.assertEqual(self.user_nt.user_type, "NonTertiary")
        self.assertEqual(models.NonTertiaryProfile.objects.count(), 1)
    
    

class AlumniSignUpFormTestCase(SignUpTestCase):
    
    def test_alumni_form_invalid(self):
        form = forms.UserRegistrationForm(data={})

        assert form.is_valid() is False, ("Should ne invalid if no data is given")

    def test_alumni_form_is_valid(self):
        response = self.client.post(reverse('account_signup'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, template_name="account/signup.html")
        
        self.user_t.refresh_from_db()
        data = {
            "email": "al@idea.com",
            "password1": self.user_al.password,
            "password2": self.user_al.password,
            "user_type": self.user_al.user_type,
        }
        form = forms.UserRegistrationForm(data)
        print(form.errors)
        self.assertTrue(form.is_valid())
        response = self.client.post(reverse('account_signup'), data=data)
        self.assertEqual(self.user_al.user_type, "Alumni")
        self.assertEqual(models.AlumniProfile.objects.count(), 1)
        
        

class TertiarySignUpFormTestCase(SignUpTestCase):
    
    def test_tertiary_form_invalid(self):
        form = forms.UserRegistrationForm(data={})

        assert form.is_valid() is False, ("Should ne invalid if no data is given")

    def test_tertiary_form_is_valid(self):
        response = self.client.post(reverse('account_signup'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, template_name="account/signup.html")
        
        
        data = {
            "email": "al@idea.com",
            "password1": self.user_t.password,
            "password2": self.user_t.password,
            "user_type": self.user_t.user_type,
        }
        form = forms.UserRegistrationForm(data=data)
        print(form.errors)
        self.assertTrue(form.is_valid())
        response = self.client.post(reverse('account_signup'), data=data)
        self.assertEqual(self.user_t.user_type, "Tertiary")
        self.assertEqual(models.TertiaryProfile.objects.count(), 1)