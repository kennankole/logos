import pytest
from django.contrib.auth import get_user
from django.contrib.messages.middleware import MessageMiddleware
from django.contrib.sessions.middleware import SessionMiddleware
from django.http import HttpRequest
from django.test import Client, RequestFactory, TestCase
from django.urls import reverse
from mixer.backend.django import mixer

from apps.usersApp import models, views

pytestmark = pytest.mark.django_db


class ProfileTestCase(TestCase):
   

    def setUp(self):
        
        self.factory = RequestFactory()

        # Different user types 
        self.user_mentor = mixer.blend(models.CustomUser, user_type="Mentor")
        self.user_m = models.MentorProfile.objects.create(user=self.user_mentor)

        self.user_ter = mixer.blend(models.CustomUser, user_type="Tertiary")
        self.user_t = models.TertiaryProfile.objects.create(user=self.user_ter)

        self.user_al = mixer.blend(models.CustomUser, user_type="Alumni")
        self.user_alu = models.AlumniProfile.objects.create(user=self.user_al)

        self.user_not = mixer.blend(models.CustomUser, user_type="NonTertiary")
        self.user_note = models.NonTertiaryProfile.objects.create(user=self.user_not)

        return super().setUp()

    def tearDown(self):
        models.CustomUser.objects.all().delete()


    def test_mentor_profile_created(self):
        self.client.force_login(self.user_mentor)

        request = self.factory.get(reverse("usersApp:mentor_detail"))
        request.session = self.client.session

        request.user = get_user(request)
        self.assertEqual(request.user, self.user_mentor)
        self.assertTrue((self.user_mentor.user_type == "Mentor") and self.user_mentor.mentorprofile)
        
        response = views.MentorProfileDetailView.as_view()(request)
        self.assertEqual(response.status_code, 200)


    def test_tertiary_profile_created(self):
        self.client.force_login(self.user_ter)

        request = self.factory.get(reverse("usersApp:tertiary_detail"))
        request.session = self.client.session

        request.user = get_user(request)
        self.assertEqual(request.user, self.user_ter)
        self.assertTrue((self.user_ter.user_type == "Tertiary") and self.user_ter.tertiaryprofile)

        response = views.TertiaryProfileDetailView.as_view()(request)
        self.assertEqual(response.status_code, 200)


    def test_non_tertiary_profile_created(self):
        self.client.force_login(self.user_not)

        request = self.factory.get(reverse("usersApp:nontertiary_detail"))
        request.session = self.client.session

        request.user = get_user(request)
        self.assertEqual(request.user, self.user_not)
        self.assertTrue((self.user_not.user_type == "NonTertiary") and self.user_not.nontertiaryprofile)
        
        response = views.NonTertiaryProfileDetailView.as_view()(request)
        self.assertEqual(response.status_code, 200)

    
    def test_alumni_profile_created(self):
        self.client.force_login(self.user_al)

        request = self.factory.get(reverse("usersApp:alumni_detail"))
        request.session = self.client.session

        request.user = get_user(request)
        self.assertEqual(request.user, self.user_al)
        self.assertTrue((self.user_al.user_type == "Alumni") and self.user_al.alumniprofile)

        response = views.AlumniProfileDetailView.as_view()(request)
        self.assertEqual(response.status_code, 200)