from django.contrib.auth import get_user


class ProfileGetObjectMixin:
    # pass

    def get_object(self, queryset=None):
        current_user = get_user(self.request)
        if current_user.user_type == "Mentor":
            return current_user.mentorprofile
        elif current_user.user_type == "Tertiary":
            return current_user.tertiaryprofile
        elif current_user.user_type == "NonTertiary":
            return current_user.nontertiaryprofile
        elif current_user.user_type == "Alumni":
            return current_user.alumniprofile