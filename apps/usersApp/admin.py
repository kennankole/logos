from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from . import models
from .forms import CustomUserChangeForm, CustomUserCreationForm

# Register your models here.

class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = models.CustomUser
    list_display = (
        'email', 'is_staff',
        'is_active',"user_type",
    )
    list_display = (
        'email', 'is_staff',
        'is_active', "user_type",
    )
    fieldsets = (
        (None, {
            "fields": ('email', 'password'),
        }),
        ('Permissions', {
            'fields': ('is_staff', 'is_active')
        }),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',), 
            'fields': ('email', 'password1', 'password2', 'is_staff', 'is_active')
        }),
    )
    search_fields = ('email', )
    ordering = ('email', )
admin.site.register(models.CustomUser, CustomUserAdmin)
admin.site.register(models.AlumniProfile)
admin.site.register(models.MentorProfile)
admin.site.register(models.TertiaryProfile)
admin.site.register(models.NonTertiaryProfile)