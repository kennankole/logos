import datetime

from django.conf import settings
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.db import models
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import gettext as _

from .managers import CustomUserManager


class CustomUser(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(_("email address"), unique=True)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    # is_mentor = models.BooleanField(default=False)
    # is_tertiary = models.BooleanField(default=False)
    # not_tertiary = models.BooleanField(default=False)
    # alumni = models.BooleanField(default=False)
    date_joined = models.DateTimeField(default=timezone.now)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    USER_TYPES = (
        ("Mentor", 'Mentor'),
        ("Tertiary", "Tertiary"),
        ("NonTertiary", "NonTertiary"),
        ("Alumni", "Alumni"),
    )
    user_type = models.CharField(max_length=20, choices=USER_TYPES)

    objects = CustomUserManager()

    def __str__(self):
        return self.email


GENDER_CHOICE = [
    ('female', 'Female'),
    ('male', 'Male'),
]   
ORPHAN_OR_NOT = (
    ("not_orphan", "Not Orphan"),
    ("orphan", "Orphan"),
    ("total_orphan", 'Total Orphan'),
) 
SECONDARY = 'Sec'
PRIMARY = 'Pri'

CHOICES = [
    (SECONDARY, 'Secondary'),
    (PRIMARY, 'Primary'),
]
TYPE_OF_INSTITUTION = [
    ('college', 'College'),
    ('university', 'University'),
    ('polytechnic', 'Polytechic')
]

TYPE_OF_DEGREE = [
    ('degree', 'Degree'),
    ('diploma', 'Diploma')
]
ALUMNI_CHOICE = [
    ('diploma', 'Diploma'),
    ('degree', 'Degree'),
    ('masters', "Masters"),
]


COUNTY = [
    ('mombasa', 'Mombasa'),
    ('kwale', 'Kwale'),
    ('kilifi', 'Kilifi'),
    ('tana_iver', 'Tana River'),
    ('lamu', 'Lamu'),
    ('taita_taveta', 'Taita Taveta'),
    ('garrisa', 'Garrisa'),
    ('wajir', 'Wajir'),
    ('mandera', 'Mandera'),
    ('isiolo', 'Isiolo'),
    ('meru', 'Meru'),
    ('tharak_nithi', 'Tharak Nithi'),
    ('embu', 'Embu'),
    ('kitui', 'Kitui'),
    ('machakos', 'Machakos'),
    ('makueni', 'Makueni'),
    ('nyandarua', 'Nyandarua'),
    ('nyeri', 'Nyeri'),
    ('kirinyanga', 'Kirinyanga'),
    ("murang'a", "Murang'a"),
    ('kiambu', 'Kiambu'),
    ('turkana', 'Turkana'),
    ('west_pokot', 'West Pokot'),
    ('samburu', 'Samburu'),
    ('trans_nzoia', 'Trans Nzoia'),
    ('uasin_gishu', 'Uasin Gishu'),
    ('elgeyo_marakwet', 'Elgeyo Marakwet'),
    ('nandi', 'Nandi'),
    ('baringo', 'Baringo'),
    ('laikipia', 'Laikipia'),
    ('nakuru', 'Nakuru'),
    ('narok', 'Narok'),
    ('kajiado', 'Kajiado'),
    ('kericho', 'Kericho'),
    ('bomet', 'Bomet'),
    ('kakamega', 'Kakamega'),
    ('vihiga', 'Vihiga'),
    ('bungoma', 'Bungoma'),
    ('busia', 'Busia'),
    ('siaya', 'Siaya'),
    ('kisumu', 'Kisumu'),
    ('homa_bay', 'Homa Bay'),
    ('migori', 'Migori'),
    ('kisii', 'Kisii'),
    ('nyamira', 'Nyamira'),
    ('nairobi', 'Nairobi')

]

STATUS_CHOICES = (
    ('send', 'Send'),
    ('accepted', 'Accepted'),
)


class TertiaryProfile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    slug = models.SlugField(max_length=30)
    # school information
    course = models.CharField(max_length=100)
    name_of_school = models.CharField(max_length=254)
    county = models.CharField(max_length=50, choices=COUNTY)
    type_of_course = models.CharField(max_length=50, choices=TYPE_OF_DEGREE)
    type_of_institution = models.CharField(max_length=50, choices=TYPE_OF_INSTITUTION)
    year_of_graduation = models.DateField(default=datetime.date.today)
    fees_structure = models.FileField(upload_to='fees_structures/%Y/%m/%d')
    helb_beneficiary = models.BooleanField(_("Are you a HELB beneficiary?(Tick/Untick to confirm)"),default=True)
    kcse_results_slip = models.FileField(upload_to='results_slips/%Y/%m/%d', default='results.jpg')
    transcript = models.FileField(upload_to='transcripts/%Y/%m/%d', default='results.pdf')

    # personal info
    date_of_birth = models.DateField(default=datetime.date.today)
    gender = models.CharField(max_length=30, choices=GENDER_CHOICE)
    birth_certificate = models.FileField(upload_to='birth_certificates/%Y/%m/%d', default='birth.jpg')
    photo = models.ImageField(upload_to="profille_pics/%Y/%m/%d")
    parents = models.CharField(max_length=15, choices=ORPHAN_OR_NOT)
    parents_or_guardian_contacts = models.PositiveIntegerField(null=True)
    contacts = models.PositiveIntegerField(null=True)
    about = models.TextField()
   
    def get_absolute_url(self):
        return reverse("usersApp:tertiary_public_profile", kwargs={"slug": self.slug})

    def get_update_url(self):
        return reverse("usersApp:tertiary_update_profile")
    
    def __str__(self) -> str:
        return self.name_of_school + self.course

class NonTertiaryProfile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    slug = models.SlugField(max_length=30)

    # personal info
    date_of_birth = models.DateField(default=datetime.date.today)
    gender = models.CharField(max_length=30, choices=GENDER_CHOICE)
    birth_certificate = models.FileField(upload_to='birth_certificates/%Y/%m/%d', default='birth.jpg')
    parents = models.CharField(max_length=15, choices=ORPHAN_OR_NOT)
    parents_or_guardian_contacts = models.PositiveIntegerField(null=True)
    about = models.TextField()

    # school info
    name_of_school = models.CharField(max_length=150)
    primary_or_secondary = models.CharField(max_length=30, choices=CHOICES)
    photo = models.ImageField(upload_to="profille_pics/%Y/%m/%d")
    county = models.CharField(max_length=50, choices=COUNTY)
    year_of_national_examination = models.DateField(default=datetime.date.today)
    fees_structure = models.FileField(upload_to='fees_structures/%Y/%m/%d')
    kcpe_results_slip = models.FileField(upload_to='results_slips/%Y/%m/%d', default='results.jpg')
    report_form = models.FileField(upload_to='transcripts/%Y/%m/%d', default='results.pdf')

    def get_absolute_url(self):
        return reverse("usersApp:nontertiary_public_profile", kwargs={"slug": self.slug})

    def get_update_url(self):
        return reverse("usersApp:nontertiary_profile_update")

    def __str__(self) -> str:
        return self.name_of_school

class MentorProfile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    occupation = models.CharField(max_length=150)
    about = models.TextField(max_length=250)
    slug = models.SlugField(max_length=30)
    photo = models.ImageField(upload_to="profille_pics/%Y/%m/%d")

    def get_absolute_url(self):
        return reverse("usersApp:mentor_public_profile", kwargs={"slug": self.slug})

    def get_update_url(self):
        return reverse("usersApp:mentor_update_profile")

    def __str__(self) -> str:
        return self.occupation
    

class AlumniProfile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    slug = models.SlugField(max_length=30)
    photo = models.ImageField(upload_to="profille_pics/%Y/%m/%d")
    occupation = models.CharField(max_length=100, default='Software engineer')
    about = models.TextField()
    study_level = models.CharField(max_length=20, choices=ALUMNI_CHOICE)
    year_of_graduation = models.DateField(default=datetime.date.today)

    def get_absolute_url(self):
        return reverse("usersApp:alumni_public_profile", kwargs={"slug": self.slug})

    def get_update_url(self):
        return reverse("usersApp:alumni_profile_update")
    
    def __str__(self) -> str:
        return self.occupation