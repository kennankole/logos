FROM python:3.8-slim-buster
# FROM python:3.7.2-alpine

#Development settings file
ENV DJANGO_SETTINGS_MODULE=logos.settings.production

#setting a working directory 
WORKDIR /usr/src/app

#Environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV DEBUG 0

#Install pipenv
RUN pip install --upgrade pip
RUN pip install pipenv

#Installing dependencies 
COPY Pipfile Pipfile.lock /usr/src/app/
RUN pipenv install --system
# RUN pipenv install --dev --ignore-pipfile --system
# RUN pipenv install --skip-lock --system


#Copy project
COPY . .

RUN python manage.py collectstatic --noinput
# RUN python manage.py flush --no-input
RUN python manage.py makemigrations
RUN python manage.py migrate

#add and run as non-root user
#Heroku recomends this 
#https://devcenter.heroku.com/articles/container-registry-and-runtime#run-the-image-as-a-non-root-user
# RUN useradd -m myuser
# RUN addgroup -S app && adduser -S app -G app
# RUN chown -R app:app /usr/src/app/
# USER app
RUN addgroup --system myuser && adduser --system --group myuser
RUN chown -R myuser:myuser /usr/src/app && chmod -R 755 /usr/src/app
USER myuser

#run guinicorn 
CMD gunicorn logos.wsgi:application --bind 0.0.0.0:$PORT