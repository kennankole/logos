import os

import dj_database_url
from decouple import config

from .base import *
from .base import BASE_DIR, DATABASES

SITE_ID = 8
ACCOUNT_DEFAULT_HTTP_PROTOCOL = 'https'

DEBUG = config("DEBUG", default=True, cast=bool)


ALLOWED_HOSTS = config("ALLOWED_HOSTS").split(" ")

DATABASE_URL = os.environ.get("DATABASE_URL")
db_from_env = dj_database_url.config(
    default=DATABASE_URL, conn_max_age=500, ssl_require=False
)
DATABASES["default"].update(db_from_env)


MIDDLEWARE = [
    "whitenoise.middleware.WhiteNoiseMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
]

STATICFILES_STORAGE = "whitenoise.storage.CompressedManifestStaticFilesStorage"
WHITENOISE_MANIFEST_STRICT = False


EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = config("EMAIL_HOST_USER")
EMAIL_HOST_PASSWORD = config("PASSWORD")
EMAIL_PORT = 587
EMAIL_USE_TLS = True

# sendgrid
# SENDGRID_API_KEY = config('SENDGRID_API_KEY')
# EMAIL_HOST = config('EMAIL_HOST')
# EMAIL_HOST_USER = config('API_KEY')# this is exactly the value 'apikey'
# EMAIL_HOST_PASSWORD = SENDGRID_API_KEY
# EMAIL_PORT = 587
# EMAIL_USE_TLS = True
